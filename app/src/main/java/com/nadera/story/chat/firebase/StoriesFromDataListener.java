package com.nadera.story.chat.firebase;

import com.nadera.story.chat.models.Story;

import java.util.ArrayList;

public interface StoriesFromDataListener {
    public void getStoriesFromBase(ArrayList<Story> stories);
}
