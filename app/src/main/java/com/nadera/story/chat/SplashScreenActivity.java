package com.nadera.story.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.nadera.story.chat.R;
import com.nadera.story.chat.firebase.FireBaseHelper;
import com.nadera.story.chat.utils.TmpData;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;


public class SplashScreenActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Intent startingIntent = this.getIntent();
        Bundle pushData = startingIntent.getBundleExtra("push");
        if(pushData != null){
            TmpData.getInstance().getLoggerEvent().pushNotificationOpen();
            TmpData.getInstance().setBundle(pushData);
        }
                    FireBaseHelper.getData(stories ->{
                        for (int i = 0; i <stories.size() ; i++) {try{
                            TmpData.getInstance().addStory(stories.get(i));}
                            catch (Exception e){}
                        }

                  });
        GoogleApiAvailability googAvail = GoogleApiAvailability.getInstance();

        int status = googAvail.isGooglePlayServicesAvailable(this);
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        AppEventsLogger.setPushNotificationsRegistrationId(FirebaseInstanceId.getInstance().getToken());
        startActivity(intent);
        finish();

            Log.d("TAG","Data is coming");
        }
}


