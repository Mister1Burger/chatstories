package com.nadera.story.chat.utils;

public class ConfigTexts {

    public static String SHARE_HOME_TITLE ;
    public static String SHARE_HOME_TEXT;
    public static String AFTER_CLICK_TITLE;
    public static Long AFTER_CLICK_TIME;
    public static String AFTER_CLICK_BUTTON;
    public static String AD_SHARE_TITLE;
    public static String AD_SHARE_TEXT;
    public static String AD_SHARE_BUTTON;
    public static String FINISH_STORY_TITLE;
    public static String FINISH_STORY_TEXT;
    public static String RATE_US_TITLE;
    public static String RATE_US_YES_BUTTON;
    public static String RATE_US_LATER_BUTTON;
    public static String RATE_US_NEVER_BUTTON;
    public static Long BLOCK_AFTER_CLICK;

}
