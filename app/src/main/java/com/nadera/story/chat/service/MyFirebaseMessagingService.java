package com.nadera.story.chat.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import com.nadera.story.chat.MainActivity;
import com.nadera.story.chat.R;
import com.nadera.story.chat.SplashScreenActivity;
import com.nadera.story.chat.utils.Utils;
import com.facebook.notifications.NotificationsManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Bundle data = new Bundle();
        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
            data.putString(entry.getKey(), entry.getValue());
        }

        Context context = this.getApplicationContext();
        Intent defaultAction = new Intent(context, SplashScreenActivity.class)
                .setAction(Intent.ACTION_DEFAULT)
                .putExtra("push", data);

        String title = data.getString("title");
        String body = data.getString("body");

        if (!Utils.IS_APP_RUNNING) {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setVibrate(new long[]{500, 500})
                    .setSmallIcon(R.drawable.icon)
                    .setContentTitle(title == null ? "" : title)
                    .setContentText(body == null ? "Hello world" : body)
                    .setAutoCancel(true)
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setContentIntent(PendingIntent.getActivity(
                            context,
                            0,
                            defaultAction,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    ));
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(123, mBuilder.build());
        }

        if (NotificationsManager.canPresentCard(data)) {
            NotificationsManager.presentNotification(
                    this,
                    data,
                    new Intent(getApplicationContext(), MainActivity.class)
            );
        }
    }
}
