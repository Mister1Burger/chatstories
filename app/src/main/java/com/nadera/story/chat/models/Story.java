package com.nadera.story.chat.models;

import java.io.Serializable;
import java.util.Comparator;

import io.realm.RealmObject;

public class Story extends RealmObject implements Serializable {
    private String author = "author";
    private String imageUrl;
    private String name = "name";
    private Long rate = 0L;
    private String storyUrl;
    private Long id = 0L;


    public Story() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrls) {
        this.imageUrl = imageUrls;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public String getStoryUrl() {
        return storyUrl;
    }

    public void setStoryUrl(String storyUrl) {
        this.storyUrl = storyUrl;
    }

    public static class IDComparator implements Comparator<Story> {
        public int compare(Story story1, Story story2) {
            return story2.getId().compareTo(story1.getId());
        }
    }

    public static class RateComparator implements Comparator<Story> {
        public int compare(Story story1, Story story2) {
            return story2.getRate().compareTo(story1.getRate());
        }
    }


}
