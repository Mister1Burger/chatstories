package com.nadera.story.chat.RealmModule;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;

public class RealmService {

    Context context;

    public RealmService(Context context) {
        this.context = context;
    }

    public Realm init() {
        Realm.init(context);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("reminder.realm")
                .modules(new MyLibraryModule())
                .build();

        try {
            return Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e) {
            try {
                Log.e("TAG", String.valueOf(e));
                Realm.deleteRealm(realmConfiguration);
                return Realm.getInstance(realmConfiguration);
            } catch (Exception ex) {
                Log.e("TAG", String.valueOf(ex));
            }
        }

        return null;
    }

    public void saveConfigsTimer(ConfigTimerClass configTimerClass) {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        assert realm != null;
        try {
            realm.beginTransaction();
            realm.insertOrUpdate(configTimerClass);
            realm.commitTransaction();
        } catch (NullPointerException ignore) {
        }
    }

    public List<ConfigTimerClass> readConfigsTimer() {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        try {
            assert realm != null;
            RealmResults<ConfigTimerClass> list = realm.where(ConfigTimerClass.class)
                    .findAll();
            if (list == null)
                return new ArrayList<>();
            return list;
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }

    public void removeConfigsTimer(ConfigTimerClass configTimerClass) {

        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        assert realm != null;
        try {
            configTimerClass = realm.where(ConfigTimerClass.class)
                    .equalTo("ID", configTimerClass.getID())
                    .findFirst();
            if (configTimerClass != null) {
                realm.beginTransaction();
                configTimerClass.deleteFromRealm();
                realm.commitTransaction();
            }
        } catch (NullPointerException ignore) {
        }
    }

    public void onDestroy() {
        Realm realm = init();
        assert realm != null;
        realm.close();
    }
}
