package com.nadera.story.chat.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.appodeal.ads.Appodeal;
import com.nadera.story.chat.MainActivity;
import com.nadera.story.chat.R;
import com.nadera.story.chat.R2;
import com.nadera.story.chat.service.TimerService;
import com.nadera.story.chat.utils.ConfigTexts;
import com.nadera.story.chat.utils.FragmentsFlags;
import com.nadera.story.chat.utils.TmpData;
import com.nadera.story.chat.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class AdAndShareFragment extends Fragment {

    @BindView(R2.id.view_flipper)
    ViewFlipper view_flipper;
    @BindView(R2.id.show_video_btn)
    Button videoButton;
    @BindView(R2.id.twitter_ib)
    ImageButton twitter_ib;
    @BindView(R2.id.share_ib)
    ImageButton share_ib;
    @BindView(R2.id.facebook_ib)
    ImageButton facebook_ib;
    @BindView(R2.id.text1)
    TextView title;
    @BindView(R2.id.text2)
    TextView text;

    CallbackManager callbackManager;
    ShareDialog shareDialog;
    SharedPreferences preferences;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ad_share_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TmpData.getInstance().getLoggerEvent().logSentPageRewardVideo();
        if (ConfigTexts.AD_SHARE_TITLE != null) {
            title.setText(ConfigTexts.AD_SHARE_TITLE);
        }
        if (ConfigTexts.AD_SHARE_TEXT != null) {
            text.setText(ConfigTexts.AD_SHARE_TEXT);
        }
        if (ConfigTexts.AD_SHARE_BUTTON != null) {
            videoButton.setText(ConfigTexts.AD_SHARE_BUTTON);
        }
        videoButton.setOnClickListener(clickListener);
        share_ib.setOnClickListener(clickListener);
        facebook_ib.setOnClickListener(clickListener);
        twitter_ib.setOnClickListener(clickListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                String postId = result.getPostId();
                if (postId != null) {
                    ((MainActivity) getActivity()).stopService(new Intent(getActivity(), TimerService.class));
                    TimerService.TIMER_RUNNING = false;
                    Utils.FIVE_VIDEOS_WATCHED = false;
                    Utils.VIDEO_WATCHED_TIMES = 0;
                    ((MainActivity) getActivity()).removeFragment(FragmentsFlags.AD_AND_SHARE);
                    ((MainActivity) getActivity()).removeFragment(FragmentsFlags.AFTER_CLICKS);
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        if (Utils.VIDEO_WATCHED_TIMES == Utils.SHOW_VIDEO_TIMES) {
            Utils.FIVE_VIDEOS_WATCHED = true;
            TmpData.getInstance().getLoggerEvent().logSentPageSkippedShare();
            view_flipper.showNext();
            Utils.IS_AD_FRAGMENT_FLIPPER_FLIPPED = true;
        } else if (Utils.IS_AD_FRAGMENT_FLIPPER_FLIPPED && !Utils.FIVE_VIDEOS_WATCHED) {
            view_flipper.showPrevious();
        }
    }

    View.OnClickListener clickListener = v -> {
        switch (v.getId()) {
            case R.id.show_video_btn:
                videoButton.setEnabled(false);
                Utils.APP_HIDE = false;
                TmpData.getInstance().getLoggerEvent().clickRewardVideo();
                preferences = ((MainActivity) getActivity()).getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("BOTTOM_PRESSED_TIMES", Utils.BOTTOM_PRESSED_TIMES);
                editor.apply();
                ((MainActivity) getActivity()).removeFragment(FragmentsFlags.AD_AND_SHARE);
                ((MainActivity) getActivity()).removeFragment(FragmentsFlags.AFTER_CLICKS);
                if (Appodeal.isLoaded(Appodeal.REWARDED_VIDEO)) {
                    Appodeal.show(getActivity(), Appodeal.REWARDED_VIDEO);
                }
                break;
            case R.id.twitter_ib:
                try {
                    Utils.APP_HIDE = false;
                    TmpData.getInstance().getLoggerEvent().clickShareTwitter();
                    Intent intent = new Intent();
                    Uri uri = Uri
                            .parse(getResources().getString(R.string.path_to_icon));
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.text_for_twitter));
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.setType("image/jpeg");
                    intent.setPackage("com.twitter.android");
                    ((MainActivity) getActivity()).startActivityForResult(intent, Utils.TWITTER_REQUEST);
                } catch (Exception e) {
                    Toast toast = Toast.makeText(getActivity(), "You don't have this app", Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;
            case R.id.share_ib:
                try {
                    TmpData.getInstance().getLoggerEvent().clickShareOther();
                    Utils.APP_HIDE = false;
                    Utils.SECONDS_LEFT_TIMER = 0;
                    Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.text_for_other_sahre));
                    shareIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(getResources().getString(R.string.app_link_googleplay)));
                    ((MainActivity) getActivity()).startActivityForResult(Intent.createChooser(shareIntent, "Share link!"), Utils.OTHER_REQUEST);
                } catch (Exception e) {
                    Toast toast = Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;
            case R.id.facebook_ib:
                TmpData.getInstance().getLoggerEvent().clickShareFacebook();
                Utils.APP_HIDE = false;
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(getResources().getString(R.string.app_link_googleplay)))
                            .build();
                    shareDialog.show(linkContent, ShareDialog.Mode.FEED);
                }
                break;
        }
    };

    @Override
    public void onDestroy() {
        Utils.AD_SHARE_FRAGMENT_CREATED = false;
        super.onDestroy();
    }
}
