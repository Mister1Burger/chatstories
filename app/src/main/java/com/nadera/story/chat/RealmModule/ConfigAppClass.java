package com.nadera.story.chat.RealmModule;

import io.realm.RealmObject;


public class ConfigAppClass extends RealmObject {
    private boolean APP_RATED = false;
    private boolean FIVE_VIDEOS_WATCHED = false;
    private int ID = 123;

    public int getID() {
        return ID;
    }

    public boolean isAPP_RATED() {
        return APP_RATED;
    }

    public void setAPP_RATED(boolean APP_RATED) {
        this.APP_RATED = APP_RATED;
    }

    public boolean isFIVE_VIDEOS_WATCHED() {
        return FIVE_VIDEOS_WATCHED;
    }

    public void setFIVE_VIDEOS_WATCHED(boolean IS_FIVE_VIDEOS_WATCHED) {
        this.FIVE_VIDEOS_WATCHED = IS_FIVE_VIDEOS_WATCHED;
    }
}
