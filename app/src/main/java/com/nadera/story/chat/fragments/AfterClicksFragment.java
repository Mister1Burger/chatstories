package com.nadera.story.chat.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nadera.story.chat.MainActivity;
import com.nadera.story.chat.R;
import com.nadera.story.chat.R2;
import com.nadera.story.chat.service.TimerService;
import com.nadera.story.chat.utils.ConfigTexts;
import com.nadera.story.chat.utils.FragmentsFlags;
import com.nadera.story.chat.utils.TmpData;
import com.nadera.story.chat.utils.Utils;


import butterknife.BindView;
import butterknife.ButterKnife;

public class AfterClicksFragment extends Fragment {
    @BindView(R2.id.show_ad_btn)
    Button show_ad_btn;
    @BindView(R2.id.time_tv)
    TextView time_tv;
    @BindView(R2.id.text1)
    TextView title;


    CountDownTimer timer;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.after80_clicks_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (ConfigTexts.AFTER_CLICK_TITLE != null) {
            title.setText(ConfigTexts.AFTER_CLICK_TITLE);
        }
        if (ConfigTexts.AFTER_CLICK_BUTTON != null) {
            show_ad_btn.setText(ConfigTexts.AFTER_CLICK_BUTTON);
        }
        show_ad_btn.setOnClickListener(v -> {
            if (!Utils.AD_SHARE_FRAGMENT_CREATED) {
                Utils.AD_SHARE_FRAGMENT_CREATED = true;
                TmpData.getInstance().getLoggerEvent().clickShowShareAds();
                ((MainActivity) getActivity()).getFragment(FragmentsFlags.AD_AND_SHARE);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!TimerService.TIMER_RUNNING) {
            ((MainActivity) getActivity()).startService(new Intent(getActivity(), TimerService.class));
            try {
                startTimer(ConfigTexts.AFTER_CLICK_TIME * 60000);
            } catch (Exception e) {
            }
        } else {
            try {
                startTimer(Long.valueOf(TimerService.timerTime));
            } catch (Exception e) {
            }
        }

    }

    private void startTimer(long i) {
        TimerService.TIMER_RUNNING = true;
        timer = new CountDownTimer(i, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                updateStringTimer(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                TimerService.TIMER_RUNNING = false;
                if (Utils.VIDEO_WATCHED_TIMES == 1) {
                    Utils.VIDEO_WATCHED_TIMES = 0;
                }
                Utils.FIVE_VIDEOS_WATCHED = false;
                Utils.BOTTOM_PRESSED_TIMES = 1;
                try {
                    if (TmpData.getInstance().getLastFlag() == FragmentsFlags.AD_AND_SHARE) {
                        ((MainActivity) getActivity()).removeFragment(FragmentsFlags.AD_AND_SHARE);
                    }
                    ((MainActivity) getActivity()).removeFragment(FragmentsFlags.AFTER_CLICKS);
                } catch (Exception e) {
                }
            }
        }.start();
    }

    private void updateStringTimer(long timeLeft) {
        int minutes = (int) timeLeft / 60000;
        int seconds = (int) timeLeft % 60000 / 1000;
        String buildTime;
        buildTime = "" + minutes;
        buildTime += ":";
        if (seconds < 10) buildTime += "0";
        buildTime += seconds;
        time_tv.setText(buildTime);
    }

    @Override
    public void onStop() {
        try {
            timer.cancel();
        } catch (Exception e) {
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
