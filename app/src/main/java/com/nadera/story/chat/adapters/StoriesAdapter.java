package com.nadera.story.chat.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nadera.story.chat.R;
import com.nadera.story.chat.R2;
import com.nadera.story.chat.models.Story;
import com.nadera.story.chat.utils.TmpData;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoriesAdapter extends RecyclerView.Adapter<StoriesAdapter.UserListViewHolder> implements Serializable {

    private ArrayList<Story> stories;
    private StoryListener listener;
    private Context context;


    public static class UserListViewHolder extends RecyclerView.ViewHolder implements Serializable {
        @BindView(R2.id.story_avatar)
        LinearLayout linearLayout;
        @BindView(R2.id.picture)
        SquareImageView imageView;
        @BindView(R2.id.story_name)
        TextView storyName;
        @BindView(R2.id.author_name)
        TextView authorName;


        UserListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public StoriesAdapter(ArrayList<Story> stories, Context context, StoryListener listener) {
        this.stories = stories;
        this.listener = listener;
        this.context = context;

    }

    private ArrayList<Story> getStories() {
        return stories;
    }

    @Override
    public StoriesAdapter.UserListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.story_avatar, parent, false);
        UserListViewHolder pvh = new UserListViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(StoriesAdapter.UserListViewHolder holder, final int position) {

        holder.storyName.setText(getStories().get(position).getName());
        holder.authorName.setText(getStories().get(position).getAuthor());
        holder.linearLayout.setOnClickListener(view -> listener.getStoryListener(getStories().get(position)));
        Picasso.with(context).load(getStories().get(position).getImageUrl()).transform(new RoundedTransformation(20, 0)).fit().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return stories.size();
    }


    public void sortById() {
        Collections.sort(stories, new Story.IDComparator());
        notifyDataSetChanged();
    }

    public void sortByRate() {
        Collections.sort(stories, new Story.RateComparator());
        notifyDataSetChanged();
    }

    public void clear() {
        getStories().clear();
        notifyDataSetChanged();
    }

}
