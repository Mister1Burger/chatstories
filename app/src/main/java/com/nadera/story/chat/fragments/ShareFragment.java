package com.nadera.story.chat.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nadera.story.chat.MainActivity;
import com.nadera.story.chat.R;
import com.nadera.story.chat.R2;
import com.nadera.story.chat.utils.ConfigTexts;
import com.nadera.story.chat.utils.TmpData;
import com.nadera.story.chat.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;


import butterknife.BindView;
import butterknife.ButterKnife;


public class ShareFragment extends DialogFragment {
    @BindView(R2.id.facebook_ib)
    ImageButton facebook;
    @BindView(R2.id.share_ib)
    ImageButton share_ib;
    @BindView(R2.id.twitter_ib)
    ImageButton twitter;
    @BindView(R2.id.close_ib)
    ImageButton close_ib;
    @BindView(R2.id.text1)
    TextView title;
    @BindView(R2.id.text2)
    TextView text;

    CallbackManager callbackManager;
    ShareDialog shareDialog;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.share_fragment, null);
        ButterKnife.bind(this, view);
        if (ConfigTexts.SHARE_HOME_TITLE != null) {
            title.setText(ConfigTexts.SHARE_HOME_TITLE);
        }
        if (ConfigTexts.SHARE_HOME_TEXT != null) {
            text.setText(ConfigTexts.SHARE_HOME_TEXT);
        }
        builder.setView(view);
        TmpData.getInstance().getLoggerEvent().logSentShareHome();
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("TAG", "SUCCESS");
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        twitter.setOnClickListener(clickListener);
        facebook.setOnClickListener(clickListener);
        share_ib.setOnClickListener(clickListener);
        close_ib.setOnClickListener(clickListener);
        return builder.create();
    }


    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (((MainActivity) ShareFragment.this.getActivity()).isOnline()) {
                switch (v.getId()) {
                    case R.id.twitter_ib:
                        try {
                            TmpData.getInstance().getLoggerEvent().clickShareTwitterHome();
                            Intent intent = new Intent();
                            Uri uri = Uri
                                    .parse(getResources().getString(R.string.path_to_icon));
                            intent.setAction(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.text_for_twitter));
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_STREAM, uri);
                            intent.setType("image/jpeg");
                            intent.setPackage("com.twitter.android");
                            ShareFragment.this.startActivity(intent);
                        } catch (Exception e) {
                            Toast toast = Toast.makeText(ShareFragment.this.getActivity(), "You don't have this app", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        break;
                    case R.id.facebook_ib:
                        try {

                            TmpData.getInstance().getLoggerEvent().clickShareFacebookHome();
                            if (ShareDialog.canShow(ShareLinkContent.class)) {
                                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                        .setContentUrl(Uri.parse(getResources().getString(R.string.app_link_googleplay)))
                                        .build();
                                shareDialog.show(linkContent);
                            }
                        } catch (Exception e) {
                            FirebaseCrash.report(e);
                            Toast toast = Toast.makeText(ShareFragment.this.getActivity(), "Error", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        break;
                    case R.id.share_ib:
                        try {
                            TmpData.getInstance().getLoggerEvent().clickShareOtherHome();
                            Intent shareIntent = new Intent(Intent.ACTION_SEND);
                            shareIntent.setType("text/plain");
                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.text_for_other_sahre));
                            shareIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(ShareFragment.this.getResources().getString(R.string.app_link_googleplay)));
                            ShareFragment.this.startActivity(Intent.createChooser(shareIntent, "Share link!"));
                        } catch (Exception e) {
                            FirebaseCrash.report(e);
                            Toast toast = Toast.makeText(ShareFragment.this.getActivity(), "Error", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        break;
                    case R.id.close_ib:
                        ShareFragment.this.getDialog().cancel();
                        break;
                }
            } else {
                Toast toast = Toast.makeText(ShareFragment.this.getActivity(), "Check your internet connection", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    };

    @Override
    public void onCancel(DialogInterface dialog) {
        Utils.DIALOG_CREATED = false;
        super.onCancel(dialog);
    }

    @Override
    public void dismiss() {
        Utils.DIALOG_CREATED = false;
        super.dismiss();
    }

}
