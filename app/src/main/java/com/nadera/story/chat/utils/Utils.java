package com.nadera.story.chat.utils;

public class Utils {
    public static int BOTTOM_PRESSED_TIMES = 0;
    public static int VIDEO_WATCHED_TIMES = 0;
    public static long SECONDS_LEFT_TIMER = 0;
    public static boolean IS_NAV_BAR_VIEWED = true;
    public static boolean IS_AD_FRAGMENT_FLIPPER_FLIPPED = false;
    public static boolean IS_APP_RUNNING = false;
    public static boolean IS_STORY_RATED = false;
    public static final int TWITTER_REQUEST = 111;
    public static final int GOOGLE_PLAY_REQUEST = 222;
    public static final int OTHER_REQUEST = 333;
    public static boolean DIALOG_CREATED = false;
    public static  boolean AD_SHARE_FRAGMENT_CREATED = false;
    public static boolean APP_RATED = false;
    public static boolean FIVE_VIDEOS_WATCHED = false;
    public static boolean APP_HIDE = true;
    public static final int SHOW_INTERSTITIAL_AFTER_CLICK = 10;
    public static final int SHOW_VIDEO_TIMES = 1;
}
