package com.nadera.story.chat.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nadera.story.chat.MainActivity;
import com.nadera.story.chat.R;
import com.nadera.story.chat.R2;
import com.nadera.story.chat.RealmModule.ConfigAppClass;
import com.nadera.story.chat.RealmModule.RealmData;
import com.nadera.story.chat.utils.ConfigTexts;
import com.nadera.story.chat.utils.FragmentsFlags;
import com.nadera.story.chat.utils.TmpData;
import com.nadera.story.chat.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RateUsFragment extends Fragment {
    @BindView(R2.id.title)
    TextView title;
    @BindView(R2.id.yes_btn)
    Button yes_btn;
    @BindView(R2.id.later_btn)
    Button later_btn;
    @BindView(R2.id.never_btn)
    Button never_btn;

    RealmData realmData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rate_us_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        realmData = TmpData.getInstance().getRealmData();
        if (ConfigTexts.RATE_US_TITLE != null) {
            title.setText(ConfigTexts.RATE_US_TITLE);
        }
        if (ConfigTexts.RATE_US_YES_BUTTON != null) {
            yes_btn.setText(ConfigTexts.RATE_US_YES_BUTTON);
        }
        if (ConfigTexts.RATE_US_LATER_BUTTON != null) {
            later_btn.setText(ConfigTexts.RATE_US_LATER_BUTTON);
        }
        if (ConfigTexts.RATE_US_NEVER_BUTTON != null) {
            never_btn.setText(ConfigTexts.RATE_US_NEVER_BUTTON);
        }
        TmpData.getInstance().getLoggerEvent().logSentPageRateApp();
        yes_btn.setOnClickListener(clickListener);
        later_btn.setOnClickListener(clickListener);
        never_btn.setOnClickListener(clickListener);
    }

    View.OnClickListener clickListener = v -> {
        switch (v.getId()) {
            case R.id.yes_btn:
                TmpData.getInstance().getLoggerEvent().clickYesRate();
                final String appPackageName = getActivity().getPackageName();
                Utils.APP_RATED = true;
                try {
                    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)), Utils.GOOGLE_PLAY_REQUEST);
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)), Utils.GOOGLE_PLAY_REQUEST);
                }
                ((MainActivity) getActivity()).removeFragment(FragmentsFlags.RATE_US);
                break;
            case R.id.later_btn:
                TmpData.getInstance().getLoggerEvent().clickLaterRate();
                ((MainActivity) getActivity()).removeFragment(FragmentsFlags.RATE_US);
                ((MainActivity) getActivity()).getFragment(FragmentsFlags.STORIES);

                break;
            case R.id.never_btn:
                TmpData.getInstance().getLoggerEvent().clickNeverRate();
                Utils.APP_RATED = true;
                ConfigAppClass configAppClass = new ConfigAppClass();
                configAppClass.setAPP_RATED(Utils.APP_RATED);
                configAppClass.setFIVE_VIDEOS_WATCHED(Utils.FIVE_VIDEOS_WATCHED);
                realmData.saveConfigsApp(configAppClass);
                TmpData.getInstance().getInstanceInformation(realmData);
                ((MainActivity) getActivity()).removeFragment(FragmentsFlags.RATE_US);
                ((MainActivity) getActivity()).getFragment(FragmentsFlags.STORIES);
                break;
        }
    };
}
