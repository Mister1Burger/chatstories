package com.nadera.story.chat;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.appodeal.ads.Appodeal;
import com.appodeal.ads.InterstitialCallbacks;
import com.appodeal.ads.RewardedVideoCallbacks;
import com.nadera.story.chat.RealmModule.ConfigAppClass;
import com.nadera.story.chat.RealmModule.RealmData;
import com.nadera.story.chat.adapters.ChatStoryAdapter;
import com.nadera.story.chat.fragments.NoInternetFragment;
import com.nadera.story.chat.models.ChatMessage;
import com.nadera.story.chat.models.Story;
import com.nadera.story.chat.service.TimerService;
import com.nadera.story.chat.utils.ConfigTexts;
import com.nadera.story.chat.utils.FragmentsFlags;
import com.nadera.story.chat.firebase.LoggerEvent;
import com.nadera.story.chat.utils.TmpData;
import com.nadera.story.chat.RealmModule.UselessClass;
import com.nadera.story.chat.utils.Utils;
import com.nadera.story.chat.fragments.AdAndShareFragment;
import com.nadera.story.chat.fragments.AfterClicksFragment;
import com.nadera.story.chat.fragments.FinishStoryFragment;
import com.nadera.story.chat.fragments.RateUsFragment;
import com.nadera.story.chat.fragments.ShareFragment;
import com.nadera.story.chat.fragments.StoriesFragment;
import com.facebook.notifications.NotificationsManager;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R2.id.show_story_btn)
    ImageButton show_story_btn;
    @BindView(R2.id.navigation)
    BottomNavigationView navigation;
    @BindView(R2.id.chat_list_view)
    RecyclerView recyclerView;
    @BindView(R2.id.first_time_in_app)
    TextView first_time_in_app;


    private AdAndShareFragment adAndShareFragment;
    private AfterClicksFragment afterClicksFragment;
    private FinishStoryFragment finishStoryFragment;
    private RateUsFragment rateUsFragment;
    private ShareFragment shareFragment;
    private StoriesFragment storiesFragment;
    private Dialog loadingDialog;

    private Handler handler;
    private LoggerEvent loggerEvent;
    private int STORY_SIZE = 0;
    private final int CLOSE_DIALOG = 444;
    private List<ChatMessage> chatMessages;
    private ChatStoryAdapter adapter;
    private RealmData realmData;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private Handler shareHandler;
    private SharedPreferences preferences;
    private static int CHECK_TIMER = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
        String appKey = "fee50c333ff3825fd6ad6d38cff78154de3025546d47a84f";
        Appodeal.setBannerViewId(R.id.appodealBannerView);
        Appodeal.disableNetwork(this, "facebook");
        Appodeal.initialize(this, appKey, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO | Appodeal.BANNER);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setStackFromEnd(true);
        recyclerView.setLayoutManager(llm);

        shareHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case Utils.TWITTER_REQUEST:
                        if (Utils.FIVE_VIDEOS_WATCHED) {
                            Utils.FIVE_VIDEOS_WATCHED = false;
                        }
                        stopService(new Intent(getBaseContext(), TimerService.class));
                        TimerService.TIMER_RUNNING = false;
                        removeFragment(FragmentsFlags.AD_AND_SHARE);
                        removeFragment(FragmentsFlags.AFTER_CLICKS);
                        Utils.VIDEO_WATCHED_TIMES = 0;
                        getDefaultView();
                        break;
                    case Utils.OTHER_REQUEST:
                        if (Utils.FIVE_VIDEOS_WATCHED) {
                            Utils.FIVE_VIDEOS_WATCHED = false;
                        }
                        stopService(new Intent(getBaseContext(), TimerService.class));
                        TimerService.TIMER_RUNNING = false;
                        removeFragment(FragmentsFlags.AD_AND_SHARE);
                        removeFragment(FragmentsFlags.AFTER_CLICKS);
                        Utils.VIDEO_WATCHED_TIMES = 0;
                        getDefaultView();
                        break;
                    case CLOSE_DIALOG:
                        loadingDialog.dismiss();
                }
            }
        };


        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                first_time_in_app.setVisibility(View.INVISIBLE);
                removeFragment(FragmentsFlags.STORIES);
                getDefaultView();
                getMessage(TmpData.getInstance().getStory());
                STORY_SIZE = chatMessages.size();
                show_story_btn.setEnabled(true);
                adapter = new ChatStoryAdapter(chatMessages, getBaseContext());
                recyclerView.setAdapter(adapter);
                if (TmpData.getInstance().getDialog() != null) {
                    TmpData.getInstance().getDialog().dismiss();
                }
            }
        };

        navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.stories_nav:
                    if (isOnline()) {
                        getFragment(FragmentsFlags.STORIES);
                        break;
                    } else {
                        Toast toast = Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT);
                        toast.show();

                    }
                case R.id.home_nav:
                    if (isOnline()) {
                        if (TmpData.getInstance().getLastFlag() == FragmentsFlags.STORIES && TmpData.getInstance().getFlags().size() != 1)
                            removeFragment(FragmentsFlags.STORIES);
                    }
                    break;
                case R.id.share_nav:
                    if (isOnline()) {
                        if (!Utils.DIALOG_CREATED) {
                            getFragment(FragmentsFlags.SHARE);
                            Utils.DIALOG_CREATED = true;
                        }
                        break;
                    } else {
                        Toast toast = Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT);
                        toast.show();
                        break;
                    }
            }
            return false;
        });

        show_story_btn.setOnClickListener(v -> {
            if (isOnline() && adapter == null) {
                getFragment(FragmentsFlags.STORIES);
                Toast toast = Toast.makeText(this, "Chose the story", Toast.LENGTH_SHORT);
                toast.show();
            } else if (isOnline()) {
                pressedShowStoryBtn();
            } else if (adapter.getItemCount() + 1 == STORY_SIZE && !Utils.IS_STORY_RATED) {
                Toast toast = Toast.makeText(this, "This is the end of the story. Now you need internet for new stories", Toast.LENGTH_SHORT);
                toast.show();
            } else {
                UselessClass uselessClass = new UselessClass();
                realmData.saveShowingData(uselessClass);
                adapter.showOneMore();
                recyclerView.scrollToPosition(adapter.getItemCount() - 1);
            }
        });

        Appodeal.setRewardedVideoCallbacks(new RewardedVideoCallbacks() {
            @Override
            public void onRewardedVideoLoaded() {
                Log.d("Appodeal", "onRewardedVideoLoaded");
            }

            @Override
            public void onRewardedVideoFailedToLoad() {
                Log.d("Appodeal", "onRewardedVideoFailedToLoad");
            }

            @Override
            public void onRewardedVideoShown() {
                Log.d("Appodeal", "onRewardedVideoShown");
            }

            @Override
            public void onRewardedVideoFinished(int amount, String name) {

                Log.d("TAG", "VIdeo posmotreno");
            }

            @Override
            public void onRewardedVideoClosed(boolean finished) {
                Log.d("Appodeal", "onRewardedVideoClosed");
                Utils.VIDEO_WATCHED_TIMES++;
                stopService(new Intent(getBaseContext(), TimerService.class));
                TimerService.TIMER_RUNNING = false;
            }
        });

        Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
            @Override
            public void onInterstitialLoaded(boolean isPrecache) {
                Log.d("Appodeal", "onInterstitialLoaded");
            }

            @Override
            public void onInterstitialFailedToLoad() {
                Log.d("Appodeal", "onInterstitialFailedToLoad");
            }

            @Override
            public void onInterstitialShown() {
                Log.d("Appodeal", "onInterstitialShown");
            }

            @Override
            public void onInterstitialClicked() {
                Log.d("Appodeal", "onInterstitialClicked");
            }

            @Override
            public void onInterstitialClosed() {
                Log.d("Appodeal", "onInterstitialClosed");
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Appodeal.show(this, Appodeal.BANNER_VIEW);
        TmpData.getInstance().getInstanceInformation(realmData);
        first_time_in_app.setVisibility(View.INVISIBLE);
        Utils.IS_APP_RUNNING = true;
        if (!isOnline() && adapter == null) {
            getFragment(FragmentsFlags.NO_INTERNET);
            hideNavigation();
        } else if (TmpData.getInstance().getBundle() != null) {
            bundleStart();
        } else {
            normalStart();
        }
    }

    private void bundleStart() {
        Bundle pushData = TmpData.getInstance().getBundle();
        try {
            loadingDialog.show();
            TmpData.getInstance().getStory().clear();
            Story story = storyFromNotification(pushData);
            Thread thread = new Thread() {
                @Override
                public void run() {
                    parseStoryToChat(story.getStoryUrl());
                    shareHandler.sendEmptyMessage(CLOSE_DIALOG);
                }
            };
            thread.start();
            TmpData.getInstance().setModuleStory(story);
            realmData.removeData();
            realmData.saveStory(story);
            getDefaultView();
        } catch (Exception e) {
        }
    }

    public void normalStart() {
        getDefaultView();
        chatMessages = realmData.readData();
        if (chatMessages.size() != 0) {
            adapter = new ChatStoryAdapter(realmData.readData(), realmData.readShowingData(), this);
            show_story_btn.setEnabled(true);
            TmpData.getInstance().setModuleStory(realmData.readStory().get(0));
            STORY_SIZE = realmData.readData().size();
            try {
                preferences = getSharedPreferences("Service", MODE_PRIVATE);
                CHECK_TIMER = preferences.getInt("Timer left", -1);
            } catch (Exception e) {
            }
            if (CHECK_TIMER == -1) {
                try {
                    preferences = getPreferences(MODE_PRIVATE);
                    Utils.BOTTOM_PRESSED_TIMES = preferences.getInt("BOTTOM_PRESSED_TIMES", -1);
                    if (Utils.BOTTOM_PRESSED_TIMES == -1) {
                        Utils.BOTTOM_PRESSED_TIMES = adapter.getShowingMessages().size();
                    }
                } catch (Exception e) {
                }
            } else {
                SharedPreferences.Editor editor = preferences.edit();
                editor.remove("Timer left");
                editor.apply();
                Utils.BOTTOM_PRESSED_TIMES = 1;
            }
            recyclerView.setAdapter(adapter);
        } else if (isOnline() && adapter == null) {
            first_time_in_app.setVisibility(View.VISIBLE);
            getFragment(FragmentsFlags.STORIES);
        }
        if (TimerService.TIMER_RUNNING) {
            getFragment(FragmentsFlags.AFTER_CLICKS);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.IS_APP_RUNNING = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        ConfigAppClass configAppClass = new ConfigAppClass();
        configAppClass.setAPP_RATED(Utils.APP_RATED);
        configAppClass.setFIVE_VIDEOS_WATCHED(Utils.FIVE_VIDEOS_WATCHED);
        realmData.saveConfigsApp(configAppClass);
        try {
            if (Utils.APP_HIDE) {
                if (TmpData.getInstance().getLastFlag() == FragmentsFlags.AD_AND_SHARE) {
                    removeFragment(FragmentsFlags.AD_AND_SHARE);
                }
                if (TmpData.getInstance().getLastFlag() == FragmentsFlags.AFTER_CLICKS) {
                    removeFragment(FragmentsFlags.AFTER_CLICKS);
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Appodeal.onResume(this, Appodeal.BANNER);
        Utils.APP_HIDE = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.IS_APP_RUNNING = false;
    }

    @Override
    public void onBackPressed() {
        Utils.DIALOG_CREATED = false;
        backPressedAction(TmpData.getInstance().getLastFlag());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Utils.TWITTER_REQUEST && resultCode == RESULT_OK) {
            shareHandler.sendEmptyMessage(Utils.TWITTER_REQUEST);
        } else if (requestCode == Utils.OTHER_REQUEST) {
            if (Utils.SECONDS_LEFT_TIMER >= 15) {
                shareHandler.sendEmptyMessage(Utils.OTHER_REQUEST);
            }

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        NotificationsManager.presentCardFromNotification(this);
    }

    private void init() {
        TmpData.getInstance().addFlag(FragmentsFlags.STORIES);
        loggerEvent = new LoggerEvent(this);
        TmpData.getInstance().setLoggerEvent(loggerEvent);
        realmData = new RealmData(this);
        TmpData.getInstance().setRealmData(realmData);
        adAndShareFragment = new AdAndShareFragment();
        afterClicksFragment = new AfterClicksFragment();
        finishStoryFragment = new FinishStoryFragment();
        rateUsFragment = new RateUsFragment();
        shareFragment = new ShareFragment();
        storiesFragment = new StoriesFragment();
        loadingDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        loadingDialog.setContentView(R.layout.loading_chat_story);
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        fetchConfigs();
        NotificationsManager.presentCardFromNotification(this);
    }

    public void getFragment(FragmentsFlags flag) {
        TmpData.getInstance().addFlag(flag);
        switch (flag) {
            case AD_AND_SHARE:
                getFragmentManager().beginTransaction()
                        .add(R.id.fragment_layout, adAndShareFragment)
                        .commit();
                break;
            case AFTER_CLICKS:
                hideNavigation();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_layout, afterClicksFragment)
                        .commit();
                break;
            case FINISH_STORY:
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_layout, finishStoryFragment)
                        .commit();
                break;
            case RATE_US:
                hideNavigation();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_layout, rateUsFragment)
                        .commit();
                break;
            case SHARE:
                TmpData.getInstance().removeLastFlag();
                shareFragment.show(getFragmentManager(), "share_dialog");
                break;
            case STORIES:
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_layout, storiesFragment)
                        .commit();
                break;
            case NO_INTERNET:
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_layout, new NoInternetFragment())
                        .commit();

        }
    }

    private void backPressedAction(FragmentsFlags flag) {
        switch (flag) {
            case AD_AND_SHARE:
                removeFragment(FragmentsFlags.AD_AND_SHARE);
                break;
            case RATE_US:
                removeFragment(FragmentsFlags.RATE_US);
                break;
            case AFTER_CLICKS:
                getExitDialog();
                break;
            case FINISH_STORY:
                getExitDialog();
                break;
            case STORIES:
                getExitDialog();
                break;
        }

    }

    public void removeFragment(FragmentsFlags flag) {
        TmpData.getInstance().removeLastFlag();
        switch (flag) {
            case AD_AND_SHARE:
                getFragmentManager().beginTransaction()
                        .remove(adAndShareFragment)
                        .commit();
                break;
            case AFTER_CLICKS:
                getDefaultView();
                getFragmentManager().beginTransaction()
                        .remove(afterClicksFragment)
                        .commit();
                break;
            case FINISH_STORY:
                getFragmentManager().beginTransaction()
                        .remove(finishStoryFragment)
                        .commit();
                break;
            case RATE_US:
                navigation.setVisibility(View.VISIBLE);
                getFragmentManager().beginTransaction()
                        .remove(rateUsFragment)
                        .commit();
                break;
            case SHARE:
                getFragmentManager().beginTransaction()
                        .remove(shareFragment)
                        .commit();
                break;
            case STORIES:
                getDefaultView();
                getFragmentManager().beginTransaction()
                        .remove(storiesFragment)
                        .commit();
                break;
        }
    }

    private void getExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Do you want to Exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> finish())
                .setNegativeButton("No", (dialog, which) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void getDefaultView() {
        if (!Utils.IS_NAV_BAR_VIEWED) {
            navigation.setVisibility(View.VISIBLE);
            Utils.IS_NAV_BAR_VIEWED = true;
        }
        show_story_btn.setEnabled(true);
    }

    private void hideNavigation() {
        if (Utils.IS_NAV_BAR_VIEWED) {
            navigation.setVisibility(View.INVISIBLE);
            Utils.IS_NAV_BAR_VIEWED = false;
        }

        show_story_btn.setEnabled(false);
    }


    public Handler getHandler() {
        return handler;
    }


    private void getMessage(ArrayList<String> strings) {
        boolean IS_NAME_GOT = false;
        chatMessages = new ArrayList<>();
        String name = null;
        String imageUrls = null;
        String search = "http";
        for (int i = 0; i < strings.size(); i++) {
            if (strings.get(i).equals("=") && IS_NAME_GOT) {
                IS_NAME_GOT = false;
            } else if (strings.get(i).equals("=")) {
                strings.remove(i);
            } else if (!IS_NAME_GOT) {
                IS_NAME_GOT = true;
                name = strings.get(i);
            } else if (strings.get(i).contains(search)) {
                imageUrls = strings.get(i);
            } else {
                ChatMessage chatMessage = new ChatMessage();

                chatMessage.setSenderName(name);
                chatMessage.setMessageText(strings.get(i));
                if (imageUrls != null) {
                    chatMessage.setImageUrl(imageUrls);
                    imageUrls = null;
                }
                chatMessages.add(chatMessage);
                realmData.saveData(chatMessage);

            }
        }

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void pressedShowStoryBtn() {
        if (adapter.getItemCount() == STORY_SIZE && !Utils.IS_STORY_RATED) {
            getFragment(FragmentsFlags.FINISH_STORY);
        } else if (Utils.BOTTOM_PRESSED_TIMES == Utils.SHOW_INTERSTITIAL_AFTER_CLICK) {
            Utils.BOTTOM_PRESSED_TIMES++;
            preferences = getPreferences(MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("BOTTOM_PRESSED_TIMES", Utils.BOTTOM_PRESSED_TIMES);
            editor.apply();
            if (Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
                Appodeal.show(this, Appodeal.INTERSTITIAL);
            }

        } else if (Utils.BOTTOM_PRESSED_TIMES == ConfigTexts.BLOCK_AFTER_CLICK) {
            Utils.BOTTOM_PRESSED_TIMES = 1;
            getFragment(FragmentsFlags.AFTER_CLICKS);

        } else {
            try {
                Utils.BOTTOM_PRESSED_TIMES++;
                UselessClass uselessClass = new UselessClass();
                realmData.saveShowingData(uselessClass);
                adapter.showOneMore();
                recyclerView.scrollToPosition(adapter.getItemCount() - 1);
            } catch (Exception e) {
                Toast toast = Toast.makeText(this, "This is the end of the story", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    private void parseStoryToChat(String storyUrl) {
        Log.d("TAG", "\n" +
                "شركة : اهلا بك تفضل");
        try {
            URL oracle = new URL(storyUrl);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(oracle.openStream(), Charset.forName("UTF-8")));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                TmpData.getInstance().getStory().add(inputLine);
            }
            in.close();
            handler.sendEmptyMessage(0);
        } catch (MalformedURLException e) {

        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    private Story storyFromNotification(Bundle data) {
        Story story = new Story();
        story.setName(data.getString("name"));
        story.setStoryUrl(data.getString("storyUrl"));
        return story;
    }

    private void fetchConfigs() {

        long cacheExpiration = 3600;
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {

                        mFirebaseRemoteConfig.activateFetched();
                    } else {
                        Toast.makeText(MainActivity.this, "Fetch Failed",
                                Toast.LENGTH_SHORT).show();
                    }
                    setConfigText();
                });
    }

    private void setConfigText() {
        try {
            ConfigTexts.AD_SHARE_BUTTON = mFirebaseRemoteConfig.getString("ad_share_button");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.AD_SHARE_TEXT = mFirebaseRemoteConfig.getString("ad_share_text");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.AD_SHARE_TITLE = mFirebaseRemoteConfig.getString("ad_share_title");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.AFTER_CLICK_BUTTON = mFirebaseRemoteConfig.getString("after_click_button");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.AFTER_CLICK_TIME = mFirebaseRemoteConfig.getLong("after_click_time");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.AFTER_CLICK_TITLE = mFirebaseRemoteConfig.getString("after_click_title");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.FINISH_STORY_TEXT = mFirebaseRemoteConfig.getString("finish_story_text");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.FINISH_STORY_TITLE = mFirebaseRemoteConfig.getString("finish_story_title");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.RATE_US_LATER_BUTTON = mFirebaseRemoteConfig.getString("rate_us_later_button");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.RATE_US_NEVER_BUTTON = mFirebaseRemoteConfig.getString("rate_us_never_button");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.RATE_US_TITLE = mFirebaseRemoteConfig.getString("rate_us_title");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.RATE_US_YES_BUTTON = mFirebaseRemoteConfig.getString("rate_us_yes_button");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.SHARE_HOME_TEXT = mFirebaseRemoteConfig.getString("share_home_text");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.SHARE_HOME_TITLE = mFirebaseRemoteConfig.getString("share_home_title");
        } catch (Exception e) {
        }
        try {
            ConfigTexts.BLOCK_AFTER_CLICK = mFirebaseRemoteConfig.getLong("number_of_click_for_ad");
        } catch (Exception e) {
        }
    }

}
