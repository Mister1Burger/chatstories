package com.nadera.story.chat.adapters;

import com.nadera.story.chat.models.Story;

public interface StoryListener {
    void getStoryListener(Story story);
}
