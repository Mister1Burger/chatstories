package com.nadera.story.chat.firebase;

import com.nadera.story.chat.models.Story;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FireBaseHelper {
    public static DatabaseReference myRef;

    public static void getData(StoriesFromDataListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("Stories");
        myRef.keepSynced(true);
        ArrayList<Story> stories = new ArrayList<>();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i = 1;
                Map<String, Object> objectMap = (HashMap<String, Object>)
                        dataSnapshot.getValue();
                for (Object obj : objectMap.values()) {
                    if (obj instanceof Map) {
                        Map<String, Object> mapObj = (Map<String, Object>) obj;
                        Story story = new Story();
                        try {
                            story.setName((String) mapObj.get("name"));
                            if (story.getName() == null) {
                                story.setName("name");
                            }
                        } catch (Exception e) {
                        }
                        try {
                            story.setAuthor((String) mapObj.get("author"));
                            if (story.getAuthor() == null) {
                                story.setAuthor("author");
                            }
                        } catch (Exception e) {
                        }
                        try {
                            story.setImageUrl((String) mapObj.get("imageUrl"));
                        } catch (Exception e) {
                            story.setImageUrl(null);
                        }
                        try {
                            story.setRate((Long) mapObj.get("rate"));
                        } catch (Exception e) {
                            story.setRate(0L);
                        }
                        try {
                            story.setStoryUrl((String) mapObj.get("storyUrl"));
                        } catch (Exception e) {
                            story.setStoryUrl(null);
                        }
                        try {
                            story.setId((Long) mapObj.get("id"));
                            Long r = (Long) mapObj.get("id");
                            if (r == null) {
                                story.setId(1L);
                            }
                        } catch (Exception e) {
                        }
                        i++;
                        stories.add(story);
                    }
                }


                listener.getStoriesFromBase(stories);
                myRef.keepSynced(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
