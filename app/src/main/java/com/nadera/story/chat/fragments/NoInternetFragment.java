package com.nadera.story.chat.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nadera.story.chat.MainActivity;
import com.nadera.story.chat.R;
import com.nadera.story.chat.SplashScreenActivity;
import com.nadera.story.chat.utils.FragmentsFlags;


public class NoInternetFragment extends Fragment {
    CountDownTimer timer;
    boolean IS_TIMER_CREATED = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.no_internet_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!IS_TIMER_CREATED) {
            timer = new CountDownTimer(60000, 5000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (((MainActivity) getActivity()).isOnline()) {
                        Intent intent = new Intent(getActivity(), SplashScreenActivity.class);
                        startActivity(intent);
                        IS_TIMER_CREATED = true;
                        ((MainActivity) getActivity()).removeFragment(FragmentsFlags.NO_INTERNET);

                    }
                }

                @Override
                public void onFinish() {
                    Intent intent = new Intent(getActivity(), SplashScreenActivity.class);
                    startActivity(intent);
                }
            }.start();
        }
    }

    @Override
    public void onPause() {
        timer.cancel();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        timer.cancel();
        super.onDestroy();
    }
}
