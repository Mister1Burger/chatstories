package com.nadera.story.chat.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nadera.story.chat.R;
import com.nadera.story.chat.R2;
import com.nadera.story.chat.models.ChatMessage;
import com.nadera.story.chat.RealmModule.UselessClass;
import com.nadera.story.chat.utils.TmpData;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.StatsSnapshot;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatStoryAdapter extends RecyclerView.Adapter<ChatStoryAdapter.UserListViewHolder> {

    private List<ChatMessage> messages;
    private ArrayList<ChatMessage> showingMessages = new ArrayList<>();
    private String speaker;
    private int i = 1;
    private boolean firstSpeaker;
    private boolean CHECK_SPEAKER = false;
    Context context;

    public static class UserListViewHolder extends RecyclerView.ViewHolder implements Serializable {
        @BindView(R2.id.chat_story)
        LinearLayout linearLayout;
        @BindView(R2.id.message_text)
        TextView message;
        @BindView(R2.id.speaker_tv)
        TextView speakerName;
        @BindView(R2.id.image_message)
        ImageView imageView;


        UserListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public ChatStoryAdapter(List<ChatMessage> messages, Context context) {
        this.messages = messages;
        this.context = context;
        getShowingMessages().add(messages.get(0));
    }

    public ChatStoryAdapter(List<ChatMessage> messages, List<UselessClass> uselessClasses, Context context) {
        this.messages = messages;
        this.context = context;
        for (int j = 0; j <= uselessClasses.size(); j++) {
            getShowingMessages().add(messages.get(j));
        }
        i = uselessClasses.size() + 1;
        speaker = getMessages().get(0).getSenderName();
    }


    private List<ChatMessage> getMessages() {
        return messages;
    }

    public ArrayList<ChatMessage> getShowingMessages() {
        return showingMessages;
    }

    @Override
    public ChatStoryAdapter.UserListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_massege, parent, false);
        ChatStoryAdapter.UserListViewHolder pvh = new ChatStoryAdapter.UserListViewHolder(v);
        return pvh;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ChatStoryAdapter.UserListViewHolder holder, final int position) {
        setColor(holder.speakerName, getShowingMessages().get(position));
        holder.message.setText(getShowingMessages().get(position).getMessageText());
        holder.speakerName.setText(getShowingMessages().get(position).getSenderName());
        if (getShowingMessages().get(position).getImageUrl() != null) {
            Picasso.with(context).load(getShowingMessages().get(position).getImageUrl()).transform(new RoundedTransformation(20, 0)).resize(1200, 800).centerCrop().into(holder.imageView);
        }


    }

    @Override
    public int getItemCount() {
        return getShowingMessages().size();
    }

    public void showOneMore() {
        getShowingMessages().add(getMessages().get(i));
        notifyItemInserted(i);
        i++;
    }


    private void setColor(TextView textView, ChatMessage message) {
        if (!CHECK_SPEAKER) {
            CHECK_SPEAKER = true;
            firstSpeaker = true;
            speaker = message.getSenderName();
        }

        if (speaker.equals(message.getSenderName()) && firstSpeaker) {
            textView.setTextColor(Color.parseColor("#FF4081"));
        } else if (speaker.equals(message.getSenderName()) && !firstSpeaker) {
            textView.setTextColor(Color.parseColor("#FF51D6FF"));
        } else {
            speaker = message.getSenderName();
            changeSpeaker();
            setColor(textView, message);
        }
    }

    private void changeSpeaker() {
        firstSpeaker = !firstSpeaker;
    }

}
