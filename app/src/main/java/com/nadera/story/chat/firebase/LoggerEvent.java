package com.nadera.story.chat.firebase;

import android.content.Context;

import com.facebook.appevents.AppEventsLogger;

public class LoggerEvent {
    Context context;
    private AppEventsLogger mLogger;

    public LoggerEvent(Context context) {
        this.context = context;
        mLogger = AppEventsLogger.newLogger(context);
    }

    public void pushNotificationOpen() {
        mLogger.logEvent("push_notification_opened");
    }

    public void clickShowShareAds() {
        mLogger.logEvent("clicked_to_show_if_share_or_ads");
    }

    public void logSentPageRewardVideo() {
        mLogger.logEvent("reward_video_page");
    }

    public void clickRewardVideo() {
        mLogger.logEvent("click_reward_video");
    }

    public void logSentPageSkippedShare() {
        mLogger.logEvent("skipped_share_page");
    }

    public void clickShareTwitter() {
        mLogger.logEvent("clicked_share_twitter");
    }

    public void clickShareFacebook() {
        mLogger.logEvent("clicked_share_facebook");
    }

    public void clickShareOther() {
        mLogger.logEvent("clicked_share_other");
    }

    public void logSentPageRateApp() {
        mLogger.logEvent("number_rate");
    }

    public void clickYesRate() {
        mLogger.logEvent("clicked_yes_rate");
    }

    public void clickLaterRate() {
        mLogger.logEvent("clicked_later_rate");
    }

    public void clickNeverRate() {
        mLogger.logEvent("clicked_no_rate");
    }

    public void logSentShareHome() {
        mLogger.logEvent("share_button_home_page");
    }

    public void clickShareTwitterHome() {
        mLogger.logEvent("share_button_home_pages_twitter");
    }

    public void clickShareFacebookHome() {
        mLogger.logEvent("share_button_home_pages_facebook");
    }

    public void clickShareOtherHome() {
        mLogger.logEvent("share_button_home_pages_other");
    }

    public void logSentFinishStory() {
        mLogger.logEvent("finish_story_page");
    }

    public void clickShareTwitterFinishStory() {
        mLogger.logEvent("share_button_finish_story_pages_twitter");
    }

    public void clickShareFacebookFinishStory() {
        mLogger.logEvent("share_button_finish_story_pages_facebook");
    }

    public void clickShareOtherFinishStory() {
        mLogger.logEvent("share_button_finish_story_pages_other");
    }

    public void clickLikeStory() {
        mLogger.logEvent("like_button_finish_story_pages");
    }

    public void clickUnlikeStory() {
        mLogger.logEvent("unlike_button_finish_story_pages");
    }
}
