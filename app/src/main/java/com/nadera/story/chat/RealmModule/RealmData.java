package com.nadera.story.chat.RealmModule;

import android.content.Context;
import android.util.Log;

import com.nadera.story.chat.models.ChatMessage;
import com.nadera.story.chat.models.Story;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;

public class RealmData {

    Context context;

    public RealmData(Context context) {
        this.context = context;
    }

    public Realm init() {
        Realm.init(context);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("reminder.realm")
                .modules(new MyLibraryModule())
                .build();

        try {
            return Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e) {
            try {
                Log.e("TAG", String.valueOf(e));
                Realm.deleteRealm(realmConfiguration);
                return Realm.getInstance(realmConfiguration);
            } catch (Exception ex) {
                Log.e("TAG", String.valueOf(ex));
            }
        }

        return null;
    }


    public List<ChatMessage> readData() {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        try {
            assert realm != null;
            RealmResults<ChatMessage> list = realm.where(ChatMessage.class)
                    .findAll();
            if (list == null)
                return new ArrayList<>();
            return list;
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }


    public void saveData(ChatMessage chatMessages) {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        assert realm != null;
        try {
            realm.beginTransaction();
            realm.insertOrUpdate(chatMessages);
            realm.commitTransaction();
        } catch (NullPointerException ignore) {
        }
    }

    public void saveShowingData(UselessClass uselessClass) {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        assert realm != null;
        try {
            realm.beginTransaction();
            realm.insertOrUpdate(uselessClass);
            realm.commitTransaction();
        } catch (NullPointerException ignore) {
        }
    }

    public List<UselessClass> readShowingData() {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        try {
            assert realm != null;
            RealmResults<UselessClass> list = realm.where(UselessClass.class)
                    .findAll();
            if (list == null)
                return new ArrayList<>();
            return list;
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }

    public void saveStory(Story story) {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        assert realm != null;
        try {
            realm.beginTransaction();
            realm.insertOrUpdate(story);
            realm.commitTransaction();
        } catch (NullPointerException ignore) {
        }
    }

    public List<Story> readStory() {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        try {
            assert realm != null;
            RealmResults<Story> list = realm.where(Story.class)
                    .findAll();
            if (list == null)
                return new ArrayList<>();
            return list;
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }

    public void removeData() {
        Realm realm = init();
        List<ConfigAppClass> classes = new ArrayList<>();
        try {
            realm.beginTransaction();
            realm.deleteAll();
            realm.commitTransaction();
        } catch (NullPointerException ignore) {

        }
    }

    public void saveConfigsApp(ConfigAppClass configAppClass) {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        assert realm != null;
        try {
            realm.beginTransaction();
            realm.insertOrUpdate(configAppClass);
            realm.commitTransaction();
        } catch (NullPointerException ignore) {
        }
    }

    public List<ConfigAppClass> readConfigsApp() {
        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        try {
            assert realm != null;
            RealmResults<ConfigAppClass> list = realm.where(ConfigAppClass.class)
                    .findAll();
            if (list == null)
                return new ArrayList<>();
            return list;
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }

    public void removeConfigsApp(ConfigAppClass configAppClass) {

        Realm realm = init();
        File realmFile = new File(context.getFilesDir(), "reminder.realm");
        assert realm != null;
        try {
            configAppClass = realm.where(ConfigAppClass.class)
                    .equalTo("ID", configAppClass.getID())
                    .findFirst();
            if (configAppClass != null) {
                realm.beginTransaction();
                configAppClass.deleteFromRealm();
                realm.commitTransaction();
            }
        } catch (NullPointerException ignore) {
        }
    }

    public void onDestroy() {
        Realm realm = init();
        assert realm != null;
        realm.close();
    }


}
