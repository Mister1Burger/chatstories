package com.nadera.story.chat;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDex;


public  class MyApplication extends Application {
    public static SharedPreferences preferences;
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        preferences = getSharedPreferences( getPackageName() + "_preferences", MODE_PRIVATE);
    }
}
