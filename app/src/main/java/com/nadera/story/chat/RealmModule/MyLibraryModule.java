package com.nadera.story.chat.RealmModule;

import io.realm.annotations.RealmModule;

@RealmModule(library = true, allClasses = true)
public class MyLibraryModule {
}
