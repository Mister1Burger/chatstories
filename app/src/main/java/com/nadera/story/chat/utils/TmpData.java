package com.nadera.story.chat.utils;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.util.LruCache;

import com.nadera.story.chat.RealmModule.ConfigAppClass;
import com.nadera.story.chat.RealmModule.RealmData;
import com.nadera.story.chat.firebase.LoggerEvent;
import com.nadera.story.chat.models.Story;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TmpData implements Serializable {
    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
    private List<FragmentsFlags> flags = new ArrayList<>();
    private ArrayList<Story> stories = new ArrayList<>();
    private ArrayList<String> story = new ArrayList<>();
    Story moduleStory;
    private LruCache<String, Bitmap> mMemoryCache = new LruCache<>(maxMemory);
    private static TmpData instance;
    private LoggerEvent loggerEvent;
    private Dialog dialog;
    private Bundle bundle;
    private RealmData realmData;

    public static TmpData getInstance() {
        if (instance == null) {
            instance = new TmpData();
        }
        return instance;

    }

    public RealmData getRealmData() {
        return realmData;
    }

    public void setRealmData(RealmData realmData) {
        this.realmData = realmData;
    }

    public Bundle getBundle() {
        return bundle;
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }

    public void getInstanceInformation(RealmData realmData) {
        List<ConfigAppClass> uselessList = realmData.readConfigsApp();
        if (uselessList.size() != 0) {
            ConfigAppClass information = uselessList.get(0);
            Utils.APP_RATED = information.isAPP_RATED();
            Utils.FIVE_VIDEOS_WATCHED = information.isFIVE_VIDEOS_WATCHED();
            realmData.removeConfigsApp(uselessList.get(0));
        }
    }

    public Dialog getDialog() {
        return dialog;
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public LoggerEvent getLoggerEvent() {
        return loggerEvent;
    }

    public void setLoggerEvent(LoggerEvent loggerEvent) {
        this.loggerEvent = loggerEvent;
    }

    public LruCache<String, Bitmap> getmMemoryCache() {
        return mMemoryCache;
    }

    public Story getModuleStory() {
        return moduleStory;
    }

    public void setModuleStory(Story moduleStory) {
        this.moduleStory = moduleStory;
    }

    public ArrayList<String> getStory() {
        return story;
    }

    public void setStory(ArrayList<String> story) {
        this.story = story;
    }

    public List<FragmentsFlags> getFlags() {
        return flags;
    }

    public ArrayList<Story> getStories() {
        return stories;
    }

    public void setStories(ArrayList<Story> stories) {
        this.stories = stories;
    }

    public boolean addStory(Story story) {
        boolean NEW_STORY = true;
        if (getStories() != null) {
            for (int i = 0; i < getStories().size(); i++) {
                try {
                    if (story.getName().equals(getStories().get(i).getName())) {
                        NEW_STORY = false;
                    }
                } catch (Exception e) {
                }
            }
        }
        if (NEW_STORY) {
            getStories().add(story);
            return true;
        } else {
            return false;
        }
    }

    public FragmentsFlags getLastFlag() {
        Log.d("TAG", String.valueOf(flags.size() - 1));
        return flags.get(flags.size() - 1);
    }

    public void removeLastFlag() {
        flags.remove(flags.size() - 1);
    }

    public void addFlag(FragmentsFlags flag) {
        flags.add(flag);
    }


}
