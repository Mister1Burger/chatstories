package com.nadera.story.chat.utils;

import java.io.Serializable;

public enum FragmentsFlags implements Serializable {
    AD_AND_SHARE,
    AFTER_CLICKS,
    FINISH_STORY,
    RATE_US,
    SHARE,
    STORIES,
    NO_INTERNET;

}
