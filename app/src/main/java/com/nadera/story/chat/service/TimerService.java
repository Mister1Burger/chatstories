package com.nadera.story.chat.service;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateUtils;
import android.util.Log;

import com.nadera.story.chat.MainActivity;
import com.nadera.story.chat.R;
import com.nadera.story.chat.RealmModule.ConfigAppClass;
import com.nadera.story.chat.RealmModule.ConfigTimerClass;
import com.nadera.story.chat.RealmModule.RealmData;
import com.nadera.story.chat.RealmModule.RealmService;
import com.nadera.story.chat.utils.ConfigTexts;
import com.nadera.story.chat.utils.TmpData;
import com.nadera.story.chat.utils.Utils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TimerService extends Service {
    private CountDownTimer timer;
    private static Date TIME_OF_END;
    public static int timerTime;
    public static boolean TIMER_RUNNING = false;
    private SharedPreferences preferences;
    private RealmService realm;


    @Override
    public void onCreate() {
        super.onCreate();
        realm = new RealmService(this);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (ConfigTexts.AFTER_CLICK_TIME != null) {
            try {
                timerTime = (int) (ConfigTexts.AFTER_CLICK_TIME * 60000);
            } catch (Exception e) {
            }
        } else {
            getInstanceInformation();
        }
        startTime();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!TIMER_RUNNING) {
            timerTime = 0;
            TIME_OF_END = null;
            timer.cancel();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startTime() {
        TIMER_RUNNING = true;
        Date date = Calendar.getInstance().getTime();
        if (TIME_OF_END != null) {
            long diff = (int) Math.abs(TIME_OF_END.getTime() - date.getTime());
            timerTime = (int) TimeUnit.MILLISECONDS.convert(diff, TimeUnit.MILLISECONDS);
        } else {
            Calendar now = Calendar.getInstance();
            now.add(Calendar.MINUTE, timerTime / 60000);
            TIME_OF_END = now.getTime();
        }
        timer = new CountDownTimer(timerTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerTime = (int) millisUntilFinished;
                Utils.SECONDS_LEFT_TIMER++;
            }

            @Override
            public void onFinish() {
                preferences = getSharedPreferences("Service", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("Timer left", 111);
                editor.apply();
                if (!Utils.IS_APP_RUNNING) {
                    NotificationCompat.Builder builder =
                            new NotificationCompat.Builder(getBaseContext())
                                    .setVibrate(new long[]{500, 500})
                                    .setSmallIcon(R.mipmap.notif_icon)
                                    .setContentTitle(getString(R.string.app_name))
                                    .setContentText(getString(R.string.notification_text))
                                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                    int NOTIFICATION_ID = 12345;

                    Intent targetIntent = new Intent(getBaseContext(), MainActivity.class);
                    PendingIntent contentIntent = PendingIntent.getActivity(getBaseContext(), 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(contentIntent);
                    builder.setAutoCancel(true);
                    NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    assert nManager != null;
                    nManager.notify(NOTIFICATION_ID, builder.build());
                }
                Utils.SECONDS_LEFT_TIMER = 16;
                TIMER_RUNNING = false;
                timerTime = 0;
                TIME_OF_END = null;
                stopSelf();
            }
        }.start();
    }

    public void getInstanceInformation() {
        List<ConfigTimerClass> uselessList = realm.readConfigsTimer();
        if (uselessList.size() != 0) {
            ConfigTimerClass information = uselessList.get(0);
            timerTime = information.getTIME();
            TIME_OF_END = information.getEND_TIME();
            realm.removeConfigsTimer(uselessList.get(0));
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        if (timer != null) {
            timer.cancel();
        }
        ConfigTimerClass configTimerClass = new ConfigTimerClass();
        configTimerClass.setEND_TIME(TIME_OF_END);
        configTimerClass.setTIME(timerTime);
        realm.saveConfigsTimer(configTimerClass);
        PendingIntent service = PendingIntent.getService(
                getApplicationContext(),
                1001,
                new Intent(getApplicationContext(), TimerService.class),
                PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, service);
        super.onTaskRemoved(rootIntent);
    }
}

