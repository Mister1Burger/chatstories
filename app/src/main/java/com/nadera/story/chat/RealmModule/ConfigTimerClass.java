package com.nadera.story.chat.RealmModule;

import java.util.Date;

import io.realm.RealmObject;

public class ConfigTimerClass extends RealmObject {
    private Date END_TIME = null;
    private int TIME = 0;
    private int ID = 111;

    public int getID() {
        return ID;
    }

    public Date getEND_TIME() {
        return END_TIME;
    }

    public void setEND_TIME(Date END_TIME) {
        this.END_TIME = END_TIME;
    }

    public int getTIME() {
        return TIME;
    }

    public void setTIME(int TIME) {
        this.TIME = TIME;
    }
}
