package com.nadera.story.chat.fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nadera.story.chat.MainActivity;
import com.nadera.story.chat.R;
import com.nadera.story.chat.R2;
import com.nadera.story.chat.RealmModule.RealmData;
import com.nadera.story.chat.adapters.StoriesAdapter;
import com.nadera.story.chat.firebase.FireBaseHelper;
import com.nadera.story.chat.models.Story;
import com.nadera.story.chat.utils.TmpData;
import com.nadera.story.chat.utils.Utils;
import com.google.firebase.crash.FirebaseCrash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StoriesFragment extends Fragment {
    @BindView(R2.id.radio_button_one)
    RadioButton radio_button_one;
    @BindView(R2.id.radio_button_two)
    RadioButton radio_button_two;
    @BindView(R2.id.stories_list)
    RecyclerView recyclerView;
    //    @BindView(R2.id.swipe_refresh_layout)
//    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R2.id.troubles_tv)
    Button troubles_tv;

    StoriesAdapter adapter;
    ArrayList<Story> stories;
    Handler handler;
    RealmData realmData;
    Dialog dialog;
    CountDownTimer timer;
    boolean RADIO_BUTTON = true;
    static boolean REFRESHING_SUCCESS;
    static boolean REFRESH_TIMER_RUNNING;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stories_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handler = ((MainActivity) getActivity()).getHandler();
        realmData = TmpData.getInstance().getRealmData();
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.loading_chat_story);
        TmpData.getInstance().setDialog(dialog);
        radio_button_two.setOnClickListener(radioButtonClickListener);
        radio_button_one.setOnClickListener(radioButtonClickListener);
        troubles_tv.setOnClickListener(v -> refreshList());

    }

    @Override
    public void onStart() {
        super.onStart();
        troubles_tv.setVisibility(View.INVISIBLE);
        if (adapter != null) {
            recyclerView.setAdapter(adapter);
        } else {
            stories = TmpData.getInstance().getStories();
            Collections.sort(stories, new Story.RateComparator());
            adapter = new StoriesAdapter(stories, getActivity(), story -> {
                if (story.getStoryUrl() != null) {
                    TmpData.getInstance().getDialog().show();
                    TmpData.getInstance().getStory().clear();
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            parseStoryToChat(story.getStoryUrl());
                        }
                    };
                    thread.start();
                    TmpData.getInstance().setModuleStory(story);
                    realmData.removeData();
                    realmData.saveStory(story);
                    Utils.IS_STORY_RATED = false;
                }
            });
            recyclerView.setAdapter(adapter);
            if (stories.size() == 0) {
                refreshList();
            }
        }
    }

    private View.OnClickListener radioButtonClickListener = v -> {
        switch (v.getId()) {
            case R.id.radio_button_one:
                if (adapter != null) {
                    adapter.sortByRate();
                }
                radio_button_one.setBackgroundResource(R.mipmap.radio);
                radio_button_two.setBackgroundResource(R.mipmap.wb_radio_two);
                RADIO_BUTTON = true;
                break;

            case R.id.radio_button_two:
                if (adapter != null) {
                    adapter.sortById();
                }
                radio_button_two.setBackgroundResource(R.mipmap.radio_two);
                radio_button_one.setBackgroundResource(R.mipmap.wb_radio);
                RADIO_BUTTON = false;
                break;
        }
    };

    private void parseStoryToChat(String storyUrl) {
        Log.d("TAG", "\n" +
                "شركة : اهلا بك تفضل");
        try {
            URL oracle = new URL(storyUrl);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(oracle.openStream(), Charset.forName("UTF-8")));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                TmpData.getInstance().getStory().add(inputLine);
            }
            in.close();
            handler.sendEmptyMessage(0);
        } catch (MalformedURLException e) {
            FirebaseCrash.report(e);
        } catch (IOException e) {
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
    }

    private void refreshList() {
        troubles_tv.setVisibility(View.INVISIBLE);
        TmpData.getInstance().getDialog().show();
        REFRESHING_SUCCESS = false;
        REFRESH_TIMER_RUNNING = true;
        timer = new CountDownTimer(10000, 10000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (!REFRESHING_SUCCESS) {
                    REFRESH_TIMER_RUNNING = false;
                    TmpData.getInstance().getDialog().dismiss();
                    troubles_tv.setVisibility(View.VISIBLE);
                }
            }
        }.start();

        try {
            adapter.clear();
            TmpData.getInstance().getStories().clear();
            FireBaseHelper.getData(stories1 -> {
                for (int i = 0; i < stories1.size(); i++) {
                    TmpData.getInstance().addStory(stories1.get(i));
                    if (i == stories1.size() - 1) {
                        if (RADIO_BUTTON) {
                            adapter.sortByRate();
                        } else {
                            adapter.sortById();
                        }
                        TmpData.getInstance().getDialog().dismiss();
                        REFRESHING_SUCCESS = true;
                        if (REFRESH_TIMER_RUNNING) {
                            timer.cancel();
                        }

                    }
                }
            });
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Error. Check your internet connection.",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
