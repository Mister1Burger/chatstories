package com.nadera.story.chat.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nadera.story.chat.MainActivity;
import com.nadera.story.chat.R;
import com.nadera.story.chat.R2;
import com.nadera.story.chat.RealmModule.RealmData;
import com.nadera.story.chat.models.Story;
import com.nadera.story.chat.utils.ConfigTexts;
import com.nadera.story.chat.utils.FragmentsFlags;
import com.nadera.story.chat.utils.TmpData;
import com.nadera.story.chat.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FinishStoryFragment extends Fragment {
    @BindView(R2.id.unlike_story_btn)
    ImageButton unlike_story_btn;
    @BindView(R2.id.like_story_btn)
    ImageButton like_story_btn;
    @BindView(R2.id.share_ib)
    ImageButton share_ib;
    @BindView(R2.id.twitter_ib)
    ImageButton twitter_ib;
    @BindView(R2.id.facebook_ib)
    ImageButton facebook_ib;
    @BindView(R2.id.first_text)
    TextView title;
    @BindView(R2.id.second_text)
    TextView text;


    private Story story;
    DatabaseReference myRef;
    private long rate = 0;
    private RealmData realmData;
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.finish_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (ConfigTexts.FINISH_STORY_TITLE != null) {
            title.setText(ConfigTexts.FINISH_STORY_TITLE);
        }
        if (ConfigTexts.FINISH_STORY_TEXT != null) {
            text.setText(ConfigTexts.FINISH_STORY_TEXT);
        }
        story = TmpData.getInstance().getModuleStory();
        realmData = TmpData.getInstance().getRealmData();
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        like_story_btn.setOnClickListener(clickListener);
        unlike_story_btn.setOnClickListener(clickListener);
        share_ib.setOnClickListener(clickListener);
        facebook_ib.setOnClickListener(clickListener);
        twitter_ib.setOnClickListener(clickListener);
        if (story.getId() != 0) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            myRef = database.getReference().child("Stories").child("Story" + String.valueOf(story.getId())).child("rate");
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        rate = (Long) dataSnapshot.getValue();
                    } catch (Exception e) {
                        rate = 0;
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }

    View.OnClickListener clickListener = v -> {
        if (((MainActivity) getActivity()).isOnline()) {
            switch (v.getId()) {
                case R.id.like_story_btn:
                    TmpData.getInstance().getLoggerEvent().clickLikeStory();
                    rate++;
                    try {
                        myRef.setValue(rate);
                    } catch (Exception e) {
                    }
                    realmData.removeData();
                    Utils.IS_STORY_RATED = true;
                    if (!Utils.APP_RATED) {
                        ((MainActivity) getActivity()).getFragment(FragmentsFlags.RATE_US);

                    } else {
                        ((MainActivity) getActivity()).getFragment(FragmentsFlags.STORIES);
                    }

                    break;
                case R.id.unlike_story_btn:
                    TmpData.getInstance().getLoggerEvent().clickUnlikeStory();
                    rate--;
                    try {
                        myRef.setValue(rate);
                    } catch (Exception e) {
                    }
                    realmData.removeData();
                    ((MainActivity) getActivity()).getFragment(FragmentsFlags.STORIES);

                    break;
                case R.id.share_ib:
                    TmpData.getInstance().getLoggerEvent().clickShareOtherFinishStory();
                    try {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.text_for_other_sahre));
                        shareIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(getResources().getString(R.string.app_link_googleplay)));
                        startActivity(Intent.createChooser(shareIntent, "Share link!"));
                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    break;
                case R.id.twitter_ib:
                    TmpData.getInstance().getLoggerEvent().clickShareTwitterFinishStory();
                    try {
                        Uri file = Uri.parse(getResources().getString(R.string.path_to_icon));
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("image/*");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, file);
                        shareIntent.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.text_for_twitter));
                        shareIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(getResources().getString(R.string.app_link_googleplay)));
                        shareIntent.setPackage("com.twitter.android");
                        startActivity(shareIntent);
                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getActivity(), "You don't have this app", Toast.LENGTH_SHORT);
                        toast.show();

                    }
                    break;
                case R.id.facebook_ib:
                    TmpData.getInstance().getLoggerEvent().clickShareFacebookFinishStory();
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(getResources().getString(R.string.app_link_googleplay)))
                                .build();
                        shareDialog.show(linkContent);
                    }
                    break;
            }
        } else {
            Toast toast = Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT);
            toast.show();
        }
    };

    @Override
    public void onStart() {
        TmpData.getInstance().getLoggerEvent().logSentFinishStory();
        super.onStart();
    }
}
